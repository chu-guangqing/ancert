#!/bin/bash

ifconfig -a | grep 'flags=' | awk -F':' '{print $1}' | while read line; do
	echo "[root@localhost]# ethtool $line"
	ethtool $line
	echo "[root@localhost]# ethtool -i $line"
	ethtool -i $line
	echo; echo; echo
done
