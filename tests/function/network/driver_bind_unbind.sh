#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source $DIR/../../lib/shell/common.sh
source $DIR/../../lib/shell/env.sh

function get_devices() {
    devices=""
    [ -n "$INTERFACES" ] || { echo "no interface found";return 1; }
    for interface in $INTERFACES;do
        pci_num=$(ethtool -i $interface | grep bus-info | awk '{print $NF}')
        [ -n "$pci_num" ] || continue
        devices="${devices}${pci_num} "
    done
    return 0
}

function device_bind(){
    device_unbind || return 1

    echo "binding devices..."
    bind_path=/sys/bus/pci/drivers/$DRIVER/bind
    [ -e $bind_path ] || { echo "no driver path";return 1; }

    for device in $devices;do
        sh -c "echo $device > $bind_path"
        [ $? -eq 0 ] && sleep 3 || { echo "failed to bind ${device}";return 1; }
    done

    return 0
}

function device_unbind(){

    echo "unbinding devices: $INTERFACES"
    unbind_path=/sys/bus/pci/drivers/$DRIVER/unbind
    [ -e $unbind_path ] || { echo "no driver path";return 1; }

    for device in $devices;do
        sh -c "echo $device > $unbind_path"
        [ $? -eq 0 ] && sleep 3 || { echo "failed to unbind ${device}";return 1; }
    done

    return 0
}

function interface_up(){
    for interface in $INTERFACES;do
    pci_num=$(ethtool -i $interface | grep bus-info | awk '{print $NF}')
    [ -n "$pci_num" ] || { echo "cannot resume status of interface $interface";return 1; }
    echo "$devices" | grep -wq "$pci_num" || { echo "pci number of interface $interface changed";return 1; }
        
    sleep 3

    # check status
    if ! ifconfig $interface | grep -wq "UP"; then
        echo "cannot resume state of $interface"
        return 1
    fi
    done
    return 0
}

function client_run() {
    echo "start client connect"
    client_cmd="iperf3 -t $IPERF3_RUNTIME -i $IPERF3_INTERVAL -P $IPERF3_PROCESS_NUM -f g -c $lts_ip"
    run_cmd "$client_cmd" &
}

function client_run() {
    echo "start client connect"
    client_cmd="iperf3 -t $IPERF3_RUNTIME -i $IPERF3_INTERVAL -P $IPERF3_PROCESS_NUM -f g -c $lts_ip"
    run_cmd "$client_cmd" > $IPERF3_LOG &
}

function server_kill() {
    server_down="kill -9 \`pidof iperf3\` &>/dev/null"
    $ESSH $lts_ip $server_down || return 1
    return 0
}

function server_run() {
    echo "start server listen"
    server_kill
    server_cmd="iperf3 -sD"
    $ESSH $lts_ip $server_cmd &
    if [ $? -eq 0 ]; then
        echo "server listen success"
        sleep 3
        return 0
    else
        echo "server listen failed"
        return 1
    fi
}

function run_bind_unbind_test(){

    IPERF3_LOG=bind_unbind_log.iperf3

    [ -z "$LTS_IPADDRESS" ] && { echo "Error: LTS IP are required, but none are given";exit 1; }
    lts_ip=$(echo $LTS_IPADDRESS | awk '{print $1}')

    server_run
    [ $? -ne 0 ] && return 1

    if ps -ef|grep iperf3|grep -w $IPERF3_PORT >/dev/null 2>&1;then
        kill -9 `ps -ef|grep iperf3|grep -w $IPERF3_PORT|awk '{print $2}'`
    fi

    client_run 
    sleep 5

    get_devices || return 1
    device_bind || return 1
    interface_up || return 1

    wait
    server_kill || return 1

    if test -f "$IPERF3_LOG"; then
        rm -rf $IPERF3_LOG
    fi

    return 0
}

# # # # # # # # main # # # # # # # #
print_test_info "Network"
bash ../../../utils/sshconf.sh setup
trap "bash ../../../utils/sshconf.sh restore" EXIT
stress_network_env 1 $RUNTIME
run_bind_unbind_test && test_pass || test_fail
