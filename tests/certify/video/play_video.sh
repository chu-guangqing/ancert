#!/usr/bin/env bash


source ../../lib/shell/common.sh

x11bin=/usr/bin/x11perf
glxbin=/usr/bin/glxgears

function check_wayland_status() {
	if pgrep Xwayland >/dev/null 2>&1; then
		return 0
	fi
	return 1
}

function check_xserver_status() {
	if pgrep Xorg>/dev/null 2>&1 || pgrep X>/dev/null 2>&1; then
		return 0
	fi
	return 1
}

function run_x_test() {
	echo "starting x testing"
	$x11bin -repeat 2 -reps 3 -all -display "$DISPLAY"
}

function run_OpenGL_test() {
	echo "starting OpenGL testing"
	local log_file='/tmp/glxgears.output'
	:> $log_file
	$glxbin > $log_file 2>&1 &
	local glxbin_pid=$!

	sleep 15
	kill -9 $glxbin_pid
	cat $log_file
	rm -rf $log_file
}

function play_video_test() {
	echo "show x user preference:"
	xset -q
	if [ $? -ne 0 ]; then
		echo "no X server at \$DISPLAY [$DISPLAY]"
		test_fail
	fi
	if [ -z "$DISPLAY" ]; then
		echo "can not get \$DISPLAY variable, please run ancert in GUI envrionment!"
		test_fail
	fi
	echo

	check_wayland_status
	local has_wayland=$?
	check_xserver_status
	local has_xserver=$?
	if [ $has_wayland -eq 0 ]; then
		echo "graphic server is Wayland"
	elif [ $has_xserver -eq 0 ]; then
		echo "graphic server is Xorg"
	else
		echo "failed to find graphic server processor!"
		test_fail
	fi

	echo -e "\nshow display information:"
	xdpyinfo -display "$DISPLAY"
	echo

	echo -e "\nshow  X-Video extension adaptor information:"
	xvinfo -display "$DISPLAY"
	echo

	echo -e "\nquery configuration information:"
	xdriinfo
	echo

	echo -e "\nshow 3D acceleration information:"
	glxinfo -display "$DISPLAY"
	echo

	run_x_test
	sleep 3
	run_OpenGL_test
}
# # # # # # # # # # # # # # main # # # # # # # # # # # #
print_test_info "Video"
show_module_info "$MODULE"
play_video_test
test_pass
