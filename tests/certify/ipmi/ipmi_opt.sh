#!/usr/bin/env bash

source ../../lib/shell/common.sh

function check_ipmitool_cmd() {
	echo "Check ipmitool."
	if ! which ipmitool > /dev/null 2>&1; then
		echo "Dose not find ipmitool on the host, please manually install ipmitool."
		test_skip
	fi

    if ! command -v "openipmicmd" > /dev/null 2>&1; then
        echo "Dose not find OpenIPMI on the host, please manually install OpenIPMI."
        test_skip
    fi
}

function check_ipmi_service() {
	echo "Start ipmi service."
	systemctl status ipmi.service
	if [ $? -ne 0 ]; then
		systemctl stop ipmi
		systemctl start ipmi
	else
		systemctl start ipmi
	fi

	echo "Check ipmi service status."
	systemctl status ipmi.service
	if [ $? -ne 0 ]; then
		echo "Failed to start ipmi.service !"
		test_fail
	fi
}

function ipmi_interface_opt() {
	echo "Get fru info:"
	ipmitool fru
	if [ $? -ne 0 ]; then
		echo "Failed to get fru info."
		test_fail
	fi

	echo "Get chassis info:"
	ipmitool chassis status
	if [ $? -ne 0 ]; then
		echo "Failed to get chassis info."
		test_fail
	fi

	echo "Get bmc info:"
	ipmitool bmc info
	if [ $? -ne 0 ]; then
		echo "Failed to get bmc info."
		test_fail
	fi

	echo "Get sensor info:"
	ipmitool -I open sensor list
	if [ $? -ne 0 ]; then
		echo "Failed to get sensor info."
		test_fail
	fi

	echo "Get lan channel info:"
	ipmitool -I open lan print
	if [ $? -ne 0 ]; then
		echo "Failed to lan channel info."
		test_fail
	fi
}

function test_ipmi() {
	echo "Start ipmi interface test."
	check_ipmitool_cmd
	check_ipmi_service
	ipmi_interface_opt
}

# # # # # # # # # # main # # # # # # # # # #
print_test_info
show_module_info $DRIVER
test_ipmi
test_pass
