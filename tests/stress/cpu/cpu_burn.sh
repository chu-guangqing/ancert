#!/usr/bin/env bash

source ../../lib/shell/env.sh
source ./cpu_stress.sh

BASEDIR="$(cd $(dirname $0);pwd)"

function clean() {
    echo "clean $CPU_BURN_LOG"
    [ -f $CPU_BURN_LOG ] && rm -rf $CPU_BURN_LOG
    echo "clean $CPU_STRESS_TEST_LOG"
    [ -f $CPU_STRESS_TEST_LOG ] && rm -rf $CPU_STRESS_TEST_LOG

    if pidof cpuburn >/dev/null 2>&1;then
        echo "kill cpuburn process"
        kill -9 `pidof cpuburn`
    fi
    echo "clean ${BASEDIR}/nohup.out"
    [ -f ${BASEDIR}/nohup.out ] && rm -rf ${BASEDIR}/nohup.out
}

trap "clean" EXIT

# # # # # # # # # # # # # # main # # # # # # # # # # # #
stress_cpu_env "cpuburn" $RUNTIME

cpu_stress_test && test_pass || test_fail
