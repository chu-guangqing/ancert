#!/usr/bin/env bash

source ../../lib/shell/env.sh
source ./network_stress.sh

# # # # # # # # main # # # # # # # #
stress_network_env 5 $RUNTIME
get_available_interface "false" || { echo "fialed to get available interface!";exit 1; }
trap "sh ./../../../utils/sshconf.sh restore" EXIT

run_iperf3_stress_test && test_pass || test_fail
