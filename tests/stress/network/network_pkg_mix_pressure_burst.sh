#!/usr/bin/env bash

source ../../lib/shell/env.sh
source ./network_stress.sh

# # # # # # # # main # # # # # # # #
stress_network_env 5 $(echo "($RUNTIME - (5*2*10)) /5 /2"|bc)
IPERF3_INTERVAL=$(echo "($RUNTIME - (5*2*10)) /5 /2 /10"|bc)
get_available_interface "false" || { echo "fialed to get available interface!";exit 1; }
trap "sh ./../../../utils/sshconf.sh restore" EXIT

for ((i=1;i<=5;i++));do
    for info in {10M:1000,100M:1000000};do
        echo -e "\n[network Large package mixed pressure test and burst and network basic stress--->$i]"
        IPERF3_TRANSMIT_RATE="${info%%:*}"
        IPERF3_TRANSMIT_SIZE="${info##*:}"
        run_iperf3_stress_test || test_fail
    done
done
test_pass
