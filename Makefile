DESTDIR=$(shell rpm -E %{_datadir})/$(shell cat ancert.spec|egrep "Name"|awk '{print $$NF}')
SUBDIRS := $(shell ls -l |grep '^d'|awk '{print $$NF}')

.PHONY: clean install


define common
@if [ ! -z $(DESTDIR) ];then \
    { [ ! -d $(DESTDIR) ] && mkdir -p -m 755 $(DESTDIR); }; \
    { cp -avrf ./ancert  $(DESTDIR)/; }; \
    { cp -avrf ./README.md  $(DESTDIR)/; }; \
    { cp -avrf ./LICENSE  $(DESTDIR)/; }; \
    { cp -avrf ./*.spec  $(DESTDIR)/; }; \
    { for dir in $(SUBDIRS);do $(MAKE) -C $$dir DESTDIR=$(DESTDIR) install;done; }; \
else\
    echo "Error: There is no target path \$$DESTDIR to operate on"; \
    exit 1; \
fi
endef


install:
	$(call common)
	@ln -s $(DESTDIR)/ancert /usr/bin/ancert

post_install:
	$(call common)	

uninstall: clean
	rm -rf /usr/bin/ancert

clean:
	@if [ ! -z $(DESTDIR) ];then \
        	echo "rm -rf \$$(DESTDIR)/*........"; \
        	{ for dir in $(SUBDIRS);do mkdir -p -m 755 $(DESTDIR)/$$dir ;done; }; \
        	{ for dir in $(SUBDIRS);do $(MAKE) -C $$dir DESTDIR=$(DESTDIR) clean;done; }; \
        	{ rm -rf $(DESTDIR)/*.spec; }; \
        	{ rm -rf $(DESTDIR)/README.md; }; \
        	{ rm -rf $(DESTDIR)/ancert; }; \
        	{ rm -rf $(DESTDIR); }; \
    	else\
        	echo "Error: There is no target path \$$DESTDIR to operate on"; \
    	fi
